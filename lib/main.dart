import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:fluttertoast/fluttertoast.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Layout',
      theme: ThemeData(
        primarySwatch: Colors.blueGrey,
      ),
      home: const MyHomePage(title: 'แอปความสนใจในเรื่องต่างๆ'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final myController_user = TextEditingController();
  final myController_pwd = TextEditingController();

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    myController_user.dispose();
    myController_pwd.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Container(
        color: Colors.black54,
        constraints: BoxConstraints.expand(),
        child: SingleChildScrollView(
          child: Column(
            children: [
              CircleAvatar(
                radius: 80,
                backgroundImage: AssetImage('assets/images/wonder.jpg'),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: const [
                  Text(
                    '✪ สิ่งที่สนใจ ✪',
                    style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Text(
                    '✪สิ่งที่มุ่งหวัง✪ ',
                    style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Text(
                    '✪สิ่งที่ตามหา✪',
                    style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.all(30),
                child: Text(
                  '🐻‍❄ความสนใจของตัวคุณ‍🐻‍❄',
                  style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(15.0),
                child: TextField(
                  controller: myController_user,
                  decoration: InputDecoration(
                    hintText: ' กรอกความสนใจ ',
                    fillColor: Colors.white,
                    filled: true,
                  ),
                ),
              ),

              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      primary: Colors.green,
                      onPrimary: Colors.white,
                      padding:
                      EdgeInsets.symmetric(horizontal: 30, vertical: 10),
                      textStyle: TextStyle(fontSize: 20),
                    ),
                    onPressed: () {
                      myController_user.clear();
                      myController_pwd.clear();
                    },
                    child: Text('✉ส่ง✉️'),
                  ),

                ],
              ),
            ],
          ),
        ),
      ),
    );
  } //end build

  void displayToast() {
    String username = myController_user.text;
    String password = myController_pwd.text;

    Fluttertoast.showToast(
      msg: 'Your username: $username \n Your password: $password',
      toastLength: Toast.LENGTH_SHORT,
    );
  }
} //end class myhomepage
